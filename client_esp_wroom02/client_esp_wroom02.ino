#include <ESP8266WiFi.h>
#include "HX711.h"

const char* ssid = "pressAP";
const char* password = "pressAP1234";

const int LOADCELL_DOUT_PIN = 2;
const int LOADCELL_SCK_PIN = 3;
float units;

long zeroFactor = 0;
uint16_t calibFactor = 64;
long atmospherePress = 100000;

String answer;
bool espRunning = false;
bool serverAvail = false;
long lastTime = 0;
long curTime = 0;
uint8_t counter = 0;

HX711 scale;
IPAddress server(192, 168, 0, 1);
WiFiClient client;

void sendPressData() {
  client.connect(server, 80);
  client.println(units);
  delay(1000);
}

bool availCheck(){
  client.connect(server, 80);
  Serial.println("IS SERVER AVAILABLE?");  
  client.println("255");  
  answer = client.readStringUntil('\r');
  client.flush();
  
  if(answer.toInt() == 255){
    Serial.println("YES");
    return true;
  }
  Serial.println("NO");
  return false;
}

bool checkIfServerAvailable() {
  if (espRunning != true) {
      return availCheck();
  }
  return true;
}

void getPressVal(){
  units = (scale.get_units(10) + atmospherePress);
}

void measure() {
  if (espRunning == false) {
    lastTime = millis();
    espRunning = true;
  }
  curTime = millis();
  if (espRunning == true && (curTime - lastTime) < 30000) {
    getPressVal();
    sendPressData();
  }
  else {
    espRunning = false;
  }
}

void setPressSensor(){
  scale.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);

  scale.set_scale();                     
  scale.tare();  
  zeroFactor = scale.read_average(20);    
  scale.set_scale(calibFactor);                  
}

void setup() {
  Serial.begin(115200);
  Serial.println("CLIENT STARTING");
  pinMode(BUILTIN_LED, OUTPUT);
  digitalWrite(BUILTIN_LED, LOW);

  setPressSensor();

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    if(counter == 10){
      Serial.println("DEEP SLEEP FOR 60 SEC");
      ESP.deepSleep(10e6);
    }
    Serial.print(".");
    delay(500);
    counter++;
  }
  Serial.println("WIFI CONNECTED");
}

void loop() {
  if (checkIfServerAvailable()) {
    measure();
  }
  else {
    Serial.println("DEEP SLEEP FOR 60 SEC");
    ESP.deepSleep(10e6);
    delay(5000);
  }
}
