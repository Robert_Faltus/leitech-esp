#include <ESP8266WiFi.h>

const char* ssid = "pressAP";
const char* password = "pressAP1234";
long units = 0;

IPAddress local_IP(192,168,0,1);
IPAddress gateway(192,168,0,1);
IPAddress subnet(255,255,255,0);
// Set web server port number to 80
WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  Serial.print("Setting soft-AP configuration ... ");
  Serial.println(WiFi.softAPConfig(local_IP, gateway, subnet) ? "Ready" : "Failed!");
  
  Serial.print("Setting soft-AP ... ");
  Serial.println(WiFi.softAP(ssid, password) ? "Ready" : "Failed!");

  Serial.print("AP IP address: ");
  Serial.println(WiFi.softAPIP());
  server.begin();
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients
  if (client) {
    if (client.connected()) {
      String request = client.readStringUntil('\r');
      client.flush();
      Serial.println(request);
      long tmp = request.toInt();
      if(tmp == 255){
        client.println(255);
        Serial.println("SERVER IS AVAILABLE");
      }
      else{
        units = tmp;
        Serial.print(units);
        Serial.println(" Pa");
      }
    }
    client.stop();                         // disconnects the client
  }
}
